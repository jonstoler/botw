'use strict';

var j=function(a){for(a=0;a<4;a++)try{return a?new ActiveXObject([,"Msxml2","Msxml3","Microsoft"][a]+".XMLHTTP"):new XMLHttpRequest}catch(b){}};

// canvas and canvas context
var cvs, ctx;

// map data
var data = {};

// current viewport
var viewport = {x:0, y:0, w:750, h:625};

// map size
var map = {x:0, y:0, w:750, h:625};

var gridSize = 1;
var zoomLevel = 0;

// number of tiles drawn for each zoom level
var zoomTiles = [1, 1, 2, 8, 16, 32];

// cached image data for tiles
var tileCache = {};

// are we displaying the actual locations of things or how many there are?
var showLocations = false;

// is the mouse dragging? (ie, canvas pan?)
var dragging = false;

// what was our last dragging position? where did this drag start?
var lastPosition = {x: 0, y: 0};
var dragStart = {x: 0, y: 0};

// are our checkboxes hidden by spoiler tags?
var showSpoilers = false;

// what are we showing?
var shown = [];

// which grid tiles are disabled (hidden)?
// default: none
var disabledGridTiles = [[false]];

// titles for displaying counts
var dataTitles = {
	"fountains": "Great Fairy Fountains",
	"landmarks/landmark": "Landmarks",
	"landmarks/minor_landmark": "Minor Landmark",
	"memories": "Memories",
	"other": "Other",
	"pins/hinox": "Hinox",
	"pins/lab": "Labs",
	"pins/lynel": "Lynel",
	"pins/molduga": "Molduga",
	"pins/shrine": "Shrines",
	"pins/shrine_resurrection": "Resurrection Shrines",
	"pins/talus": "Talus",
	"pins/tower": "Towers",
	"pots": "Pots",
	"quests/mainquest": "Main Quests",
	"quests/objective": "Objectives",
	"quests/shrinequest": "Shrine Quests",
	"quests/sidequest": "Side Quests",
	"regions/region": "Regions",
	"regions/subregion": "Subregions",
	"seeds": "Korok Seeds",
	"settlements/armor": "Armories",
	"settlements/dye": "Dyes",
	"settlements/inn": "Inns",
	"settlements/jewelry": "Jewelers",
	"settlements/store": "Stores",
	"settlements/village": "Villages",
	"stables": "Stables",
	"statues": "Statues",
	"treasures": "Treasures"
}

function loadData(){
	function load(file){
		var xhr = j();
		xhr.onreadystatechange = function(){
			if(this.readyState == 4){
				data[file] = JSON.parse(this.responseText);
				render();
			}
		}
		xhr.open("GET", "data/" + file + ".json");
		xhr.overrideMimeType("application/json");
		xhr.send(null);
	}

	load("fountains");
	load("landmarks");
	load("memories");
	load("other");
	load("pins");
	load("pots");
	load("quests");
	load("regions");
	load("seeds");
	load("settlements");
	load("stables");
	load("statues");
	load("treasures");
}

// -- GEOMETRY --
function tileSize(){ return {w: map.w / zoomTiles[zoomLevel], h: map.h / zoomTiles[zoomLevel]}; }
function getVisibleElements(){
	function longLatToPoint(coordinates){
		var lat = coordinates[1];
		var long = coordinates[0];

		var x = ((long - 0.2445) / (1.2744 - 0.2445)) * map.w;
		var y = map.h - (((lat - 0.614) / (1.472 - 0.614)) * map.h);

		return {x: x, y: y};
	}

	var points = [];
	for(var i = 0; i < shown.length; i++){
		var target = shown[i];
		var split = target.split("/");
		var type = split[0];
		var subtype = split[1];
		for(var j = 0; j < data[type].length; j++){
			var obj = data[type][j];

			if(subtype == undefined || obj.properties.type == subtype){
				var coordinates = obj.geometry.coordinates;
				var point = longLatToPoint(coordinates);

				points.push({x: point.x, y: point.y, type: type, subtype: subtype, fulltype: target});
			}
		}
	}
	return points;
}
function inside(x, y, rx, ry, rw, rh){
	return (x >= rx && x <= (rx + rw) && y >= ry && y <= (ry + rh));
}
function generateGrid(){
	disabledGridTiles = [];
	for(var x = 0; x < gridSize; x++){
		disabledGridTiles[x] = [];
		for(var y = 0; y < gridSize; y++){
			disabledGridTiles[x][y] = false;
		}
	}
}

// -- RENDERING --
// paint the canvas with our map
function render(){
	ctx.fillStyle = "#4c4c4c";
	ctx.fillRect(map.x, map.y, map.w, map.h);

	if(zoomLevel == 0){
		renderGrid();
	} else {
		renderTiles();
		renderGrid();

		(showLocations ? renderLocations : renderCounts)();
	}
}

// draw grid, taking into account zoom
function renderGrid(){
	ctx.strokeStyle = "#62bae1";
	ctx.lineWidth = 1;

	var tileWidth = (zoomTiles[zoomLevel] * map.w) / gridSize;
	var tileHeight = (zoomTiles[zoomLevel] * map.h) / gridSize;
	for(var x = 0; x < gridSize; x++){
		for(var y = 0; y < gridSize; y++){
			if(disabledGridTiles[x][y] === true){
				ctx.fillStyle = "black";
				ctx.fillRect(x * tileWidth - viewport.x, y * tileHeight - viewport.y, tileWidth + 1, tileHeight + 1);
			} else {
				ctx.strokeRect(x * tileWidth - viewport.x, y * tileHeight - viewport.y, tileWidth, tileHeight);
			}
		}
	}
}

function renderTiles(){
	function draw(src, x, y){
		if(tileCache[src] !== undefined){
			ctx.drawImage(tileCache[src], x, y, map.w, map.h);
		} else {
			var tile = new Image();
			tile.onload = function(){
				tileCache[src] = tile;
				render();
			};
			tile.src = src;
		}
	}

	if(zoomLevel == 1){
		draw("tiles/1/0-0.png", 0, 0);
	} else if(zoomLevel > 1){
		var tileCount = zoomTiles[zoomLevel];

		// okay, this isn't quite lazy drawing but it's easier math
		// there can be a maximum of four partial tiles on screen at a time, so we just draw all four always
		var firstVisibleX = Math.floor(viewport.x / map.w);
		var firstVisibleY = Math.floor(viewport.y / map.h);
		var lastVisibleX = firstVisibleX + 1;
		var lastVisibleY = firstVisibleY + 1;

		for(var x = firstVisibleX; x <= lastVisibleX; x++){
			for(var y = firstVisibleY; y <= lastVisibleY; y++){
				draw("tiles/" + zoomLevel + "/" + x + "-" + y + ".png", x * map.w - viewport.x, y * map.h - viewport.y);
			}
		}
	}
}

function renderLocations(){
	ctx.fillStyle = "#62bae1";
	var visible = getVisibleElements();
	for(var i = 0; i < visible.length; i++){
		var x = visible[i].x * zoomTiles[zoomLevel] - viewport.x;
		var y = visible[i].y * zoomTiles[zoomLevel] - viewport.y;

		var gridX = Math.floor(gridSize * (x + viewport.x) / (map.w * zoomTiles[zoomLevel]));
		var gridY = Math.floor(gridSize * (y + viewport.y) / (map.h * zoomTiles[zoomLevel]));
		if(disabledGridTiles[gridX][gridY] === true){ continue; }

		ctx.beginPath();
		ctx.arc(x, y, 4, 0, 2 * Math.PI, false);
		ctx.fill();
		ctx.closePath();
	}
}

function renderCounts(){
	ctx.fillStyle = "#62bae1";

	var visible = getVisibleElements();

	var gridCount = Math.max(gridSize, 1);
	var tileWidth = (zoomTiles[zoomLevel] * map.w) / gridCount;
	var tileHeight = (zoomTiles[zoomLevel] * map.h) / gridCount;

	var counts = [];
	for(var x = 0; x < gridCount; x++){
		counts[x] = [];
		for(var y = 0; y < gridCount; y++){
			counts[x][y] = {};
			for(var i = 0; i < shown.length; i++){
				counts[x][y][shown[i]] = 0;
			}
		}
	}

	for(var i = 0; i < visible.length; i++){
		var x = Math.floor(gridCount * visible[i].x / map.w);
		var y = Math.floor(gridCount * visible[i].y / map.h);
		counts[x][y][visible[i].fulltype]++;
	}

	ctx.fillStyle = "#a4eff9";
	ctx.font = "12px sans-serif";
	for(var x = 0; x < gridCount; x++){
		for(var y = 0; y < gridCount; y++){
			if(disabledGridTiles[x][y]){ continue; }
			var line = 0;
			for(var type in counts[x][y]){
				if(counts[x][y].hasOwnProperty(type)){
					ctx.fillText(dataTitles[type] + ": " + counts[x][y][type], x * tileWidth - viewport.x + 10, y * tileHeight - viewport.y + 20 + 15 * line);
					line++;
				}
			}
		}
	}
}


// -- DOM --
window.onload = function(){
	cvs = document.getElementById("map");
	ctx = cvs.getContext("2d");

	loadData();

	var radioZoom = document.getElementsByName("zoom");
	for(var i = 0; i < radioZoom.length; i++){
		radioZoom[i].onchange = function(){
			for(var i = 0; i < radioZoom.length; i++){
				if(radioZoom[i].checked === true){
					var previousZoom = zoomLevel;
					zoomLevel = parseInt(radioZoom[i].value);
					if(zoomLevel < 2){
						viewport.x = 0;
						viewport.y = 0;
					} else {
						viewport.x = (viewport.x / (map.w * zoomTiles[previousZoom])) * map.w * zoomTiles[zoomLevel];
						viewport.y = (viewport.y / (map.h * zoomTiles[previousZoom])) * map.h * zoomTiles[zoomLevel];
					}

					render();
					return true;
				}
			}
		}
	}

	document.getElementById("form-displaymode").onchange = function(){
		showLocations = document.getElementById("displaymode-locations").checked;
		render();
	}

	document.getElementById("grid-plus").onclick = function(){
		gridSize++;
		document.getElementById("grid-size").innerText = gridSize;
		generateGrid();
		render();
	}
	document.getElementById("grid-minus").onclick = function(){
		gridSize--;
		gridSize = Math.max(0, gridSize);
		document.getElementById("grid-size").innerText = gridSize;
		generateGrid();
		render();
	}

	document.getElementById("spoil").onclick = function(){
		document.getElementById("toggles").className = (showSpoilers ? "unspoiled" : "spoiled");
		this.innerText = (showSpoilers ? "Spoil" : "Unspoil");
		showSpoilers = !showSpoilers;
	}

	document.getElementById("toggles").onchange = function(e){
		if(e.target.checked){
			shown.push(e.target.value);
		} else {
			for(var i = shown.length - 1; i >= 0; i--){
				if(shown[i] === e.target.value){
					shown.splice(i, 1);
					break;
				}
			}
		}
		render();
	}

	cvs.onmousedown = function(e){
		dragging = true;
		lastPosition = {x: e.clientX, y: e.clientY};
		dragStart = lastPosition;
	}
	cvs.onmousemove = function(e){
		if(dragging){
			var delta = {
				x: e.clientX - lastPosition.x,
				y: e.clientY - lastPosition.y
			}
			viewport.x -= delta.x;
			viewport.y -= delta.y;
			lastPosition = {x: e.clientX, y: e.clientY};

			// don't pan past ends of map
			viewport.x = Math.max(0, viewport.x);
			viewport.y = Math.max(0, viewport.y);
			viewport.x = Math.min(viewport.x, map.w * (zoomTiles[zoomLevel] - 1) - 1);
			viewport.y = Math.min(viewport.y, map.h * (zoomTiles[zoomLevel] - 1) - 1);

			// redraw after changing viewport
			render();
		}
	}
	cvs.onmouseleave = function(e){
		dragging = false;
	}
	cvs.onmouseup = function(e){
		dragging = false;
		if(Math.abs(e.clientX - dragStart.x) < 5 && Math.abs(e.clientY - dragStart.y) < 5){
			// didn't move, now it's a click
			var x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft - cvs.offsetLeft;
			var y = e.clientY + document.body.scrollTop + document.documentElement.scrollTop - cvs.offsetTop;

			var gridX = Math.floor(gridSize * (x + viewport.x) / (map.w * zoomTiles[zoomLevel]));
			var gridY = Math.floor(gridSize * (y + viewport.y) / (map.h * zoomTiles[zoomLevel]));

			disabledGridTiles[gridX][gridY] = !disabledGridTiles[gridX][gridY];
		}
		render();
	}
}