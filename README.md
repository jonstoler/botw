# Breath of the Wild: Spoiler-Free Map

**Warning: This repository is quite large and may take a long time to download, depending on your connection speed. You can use the [Gitlab Pages version](https://jonstoler.gitlab.io/botw) instead.**

> Note: This project is currently technically usable but very buggy!

This is a complete item and location map for The Legend of Zelda: Breath of the Wild, designed for players like me who might want to use a map but also want to avoid spoilers.

# Usage

By default, the map shows nothing. (No spoilers that way!)

You can create a square grid across the map by adjusting the "grid size."

To hide a grid space (say, because you haven't unlocked that area of the map yet), click on it and it will turn gray. Hidden grid spaces will only show a gray square.

> Note: for now, only hide grid spaces at zoom level 0 or 1. Hiding zoomed-in grid spaces is buggy.

By default, nothing is shown. (Not even the names of anything.) You can hover over a black bar to see its contents, or click the "Spoil" button to reveal all items at once.

To make an item visible, check it off.

By default, the map will show how many of each checked item you can find in each grid space. You can instead show their locations by clicking on "Show feature locations."

You can adjust the zoom level by using the "Zoom level" radio selection or scrolling the mouse wheel.

> Note: zooming is currently a little buggy, especially with "Show feature counts" on.

All data and assets have been ripped from [Zelda Dungeon's spoiler-full Breath of the Wild map](https://zeldadungeon.net/breath-of-the-wild-interactive-map).
